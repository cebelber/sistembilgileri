# sistembilgileri

Linux dağıtımlarında sistem bilgilerini göstermek için kodlanmış basit bir programdır.

Python ve Tkinter kullanılmıştır.

sistemBilgileri.py ile aynı dizin içine Linux dağıtımlarının logolarını kopyalarsanız program çalıştığı zaman logo değişir. Aksi halde tux.png dosyası gösterilir. 

#translated by google
A simple program written in Python and Tkinter

It is a simple program coded to show system information in Linux distributions.

Python and Tkinter are used.

If you copy the logos of Linux distributions into the same directory as sistemBilgileri.py, the logo changes when the program runs. Otherwise, the tux.png file is shown.

<img src="screenshot.png">
