"""
Sistem bilgilerini gösteren basit bir program.
Python ve Tkinter ile kodlanmıştır.
akarsu@protonmail.com

A simple program that displays system information.
It is coded in Python and Tkinter.
"""
# !/usr/bin/python3
import socket
import distro
import psutil
import platform
import os
from tkinter import *
from tkinter import ttk
from datetime import datetime
import locale

locale.setlocale(locale.LC_ALL, '')

sistem = Tk()
sistem.geometry("600x630")
sistem.title("Bilgisayar Bilgileri")
sistem.resizable(width=False, height=False)
renk = "#e2e2e2"
sistem.configure(background=renk)
sistem.iconphoto(False, PhotoImage(file='tux.png'))


try:
    img = PhotoImage(file=distro.id()+'.png')

except:
    img = PhotoImage(file='tux.png')

# banner at the top (turkuaz - #23aea3) (blue #237ae4)
Label(sistem, width=125, height=5, bg="#23aea3").place(x=0, y=55)
line = ttk.Separator(sistem, orient='horizontal', ).place(x=40, y=4, relwidth=0.85)

#Distro logo
canvas = Canvas(sistem, width=150, height=150, highlightthickness=1, highlightbackground="#e2e2e2", background="white")
canvas.place(x=220, y=24)
canvas.create_image(15, 20, anchor=NW, image=img)

# distro name
distro_name = Label(sistem, background=renk, text=distro.name() + " " + distro.version(), font=("dejavu", 26))
distro_name.place(x=40, y=200)

line1 = ttk.Separator(sistem, orient='horizontal', ).place(x=40, y=250, relwidth=0.85)

# show date
bugun = datetime.today()
print(str(bugun.day)+"."+str(bugun.month)+"."+str(bugun.year)+" "+datetime.strftime(bugun, '%A'))
Label(sistem, background=renk, font=("dejavu", 14), text=str(bugun.day)+'.'+str(bugun.month)+'.'+str(bugun.year)+' / '+datetime.strftime(bugun, '%A')).place(x=40, y=270)

#user name and server name
user_name = Label(sistem, background=renk, text=os.getlogin() + "@" + platform.node(), font=("dejavu", 14))
user_name.place(x=40, y=300)

# kernel info
kernel_lbl = Label(sistem, background=renk, text='Kernel : ' + os.popen('uname -r').read(), font=("dejavu", 12))
kernel_lbl.place(x=40, y=330)

# desktop manager
Label(sistem, background=renk, text='Desktop : '+os.popen('echo $XDG_CURRENT_DESKTOP').read(), font=("dejavu", 12,)).place(x=40, y=360)

# cpu info
with open("/proc/cpuinfo", "r") as dosya:
    for i in dosya.readlines():
        if i.find("model name") != -1:
            cpu_info = i
            break

cpu_name = cpu_info.rstrip("\n")
cpu_name = cpu_name.lstrip("model name :")

# number of cpu
cpu1 = os.popen("echo $(grep -c '^processor' /proc/cpuinfo)").read()

cpu_count = Label(sistem, background=renk, text="CPU : " + cpu_name[3:] + " X " + cpu1, font=("dejavu", 12))
cpu_count.place(x=40, y=390)

# graphic card info
gpu_info = os.popen('glxinfo | grep "OpenGL renderer string" | cut -d \':\' -f2 | xargs').read()
gpu_name = Label(sistem, background=renk, text="GPU : "+gpu_info, font=("dejavu", 12))
gpu_name.place(x=40, y=420)

# RAM info
ram = Label(sistem, background=renk, font=("dejavu", 12))
ram.configure(text="RAM (Toplam / Kullanılan / Boş) : " + str(round(psutil.virtual_memory().total/1024/1024/1024, 2))+" GB" + " / " + str(round(psutil.virtual_memory().used/1024/1024/1024, 2))+" GB" + " / " + str(round(psutil.virtual_memory().available/1024/1024/1024, 2))+" GB")
ram.place(x=40, y=450)

# Disk info
disk = Label(sistem, background=renk, font=("dejavu", 12))
disk.configure(text="Disk Kullanımı : " + str(round(psutil.disk_usage("/").total/1024/1024/1024, 2)) + " GB " + " / " + str(round(psutil.disk_usage("/").used/1024/1024/1024, 2)) + " GB " + " / " + str(round(psutil.disk_usage("/").free/1024/1024/1024, 2)) + " GB ")
disk.place(x=40, y=480)

# IP address
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(('8.8.8.8', 80))
ip = s.getsockname()[0]
s.close()

ipaddress = Label(sistem, background=renk, font=("dejavu", 12))
ipaddress.configure(text="Yerel IP Adresi : " + str(ip))
ipaddress.place(x=40, y=510)

# exit button
exit_btn = Button(sistem, background=renk, text="Kapat", command=sistem.destroy, bg="#55aaff", fg="#ffffff")
exit_btn.place(height=30, x=510, y=580)

# footnote
foot_note = Label(sistem, background=renk, text=(".: akarsu@protonmail.com :. "), font=("dejavu", 10))
foot_note.place(x=200, y=610)

sistem.mainloop()
